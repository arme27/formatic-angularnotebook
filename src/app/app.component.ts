import { Component } from '@angular/core';
import { appRoutes } from './app.module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'noteBook';
  menus: any = [];
  constructor(){
    let colores = ["postit-blue","postit-green","postit-red","postit-yellow","postit-pink"];
    let n = 0;
    for (let i = 0; i < appRoutes.length; i++) {
      if (appRoutes[i].name != undefined){
        this.menus.push([appRoutes[i].name, `/${appRoutes[i].path}`,colores[n]]);
        n++;
      }
    }
  }
}
